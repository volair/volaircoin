var ownerAddress = "0xa87fe66513beb69c13aaefdb2091d567d1f4acd3";

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    ropsten: {
      host: "localhost",
      port: 8545,
      gas: 4700000,
      gasPrice: 21000000000,
      network_id: "3",
      from: ownerAddress
    },
    mainnet: {
      host: "localhost",
      port: 8545,
      gas: 4700000,
      gasPrice: 10000000000,
      network_id: "1",
      from: ownerAddress
    },
  },  
  mocha: {
    reporter: 'eth-gas-reporter',
    reporterOptions : {
      currency: 'USD',
      gasPrice: 21
    }
  }
};
