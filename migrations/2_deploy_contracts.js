var VolAirCoin = artifacts.require("./VolAirCoin.sol");
var VolAirCoinCrowdsale = artifacts.require("./VolAirCoinCrowdsale.sol");

module.exports = async function(deployer) {
  // Crowdsale dates (Testnet dates)
  // const openingTime = web3.eth.getBlock('latest').timestamp + 300; 
  // const closingTime = Math.round(+new Date("2018-11-30T00:00:00Z")/1000);
  
  // Crowdsale dates (Mainnet dates)
  const openingTime = Math.round(+new Date("2018-09-26T00:00:00Z")/1000);
  const closingTime = Math.round(+new Date("2018-11-30T00:00:00Z")/1000);
  
  // Crowdsale dates (testing dates)
  // const openingTime = web3.eth.getBlock('latest').timestamp + 120; // secs in the future
  // const closingTime = openingTime + (1000 * 14400);

  // Rate VOL to ETH
  const rate = new web3.BigNumber(2500);
    
  // Wallet where ETH are deposited in crowdsale
  const wallet = web3.eth.accounts[0];

  // Wallet containing VOL to distribute in crowsale
  const crowdsaleWallet = web3.eth.accounts[1];

  return deployer.deploy(VolAirCoin, { from: wallet })
    .then((result) => {
      return deployer.deploy(VolAirCoinCrowdsale, openingTime, closingTime, rate, wallet, VolAirCoin.address, crowdsaleWallet);
    })
    .catch((error) => {
      console.error(error);
    })
};
