pragma solidity ^0.4.24;

import "./ERC20/StandardToken.sol";

contract VolAirCoin is StandardToken {
    string public name = "VolAir Coin"; 
    string public symbol = "VOL";
    uint public decimals = 18;
    uint public INITIAL_SUPPLY = 500000000 * (10 ** decimals);

    constructor() public {
        totalSupply_ = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
        emit Transfer(address(0), msg.sender, INITIAL_SUPPLY);
    }
}