pragma solidity ^0.4.24;

import "./VolAirCoin.sol";
import "./crowdsale/TimedCrowdsale.sol";
import "./crowdsale/AllowanceCrowdsale.sol";
import "./crowdsale/FinalizableCrowdsale.sol";

contract VolAirCoinCrowdsale is AllowanceCrowdsale, TimedCrowdsale {    
    constructor (uint256 _openingTime, uint256 _closingTime, uint256 _rate, address _wallet, VolAirCoin _token, address _tokenWallet) public 
        Crowdsale(_rate, _wallet, _token)
        AllowanceCrowdsale(_tokenWallet)
        TimedCrowdsale(_openingTime, _closingTime)
    {
    }
}