const latestTime = require('./helpers/latestTime');
const {
  increaseTime,
  increaseTimeTo,
  duration,
} = require('./helpers/increaseTime');
const EVMRevert = 'revert';
const BigNumber = web3.BigNumber;

const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should();

const CrowdsaleContract = artifacts.require('VolAirCoinCrowdsale');
const VolAirCoin = artifacts.require('VolAirCoin');

// Inject tx watcher
web3.eth.getTransactionReceiptMined = require('./helpers/getTransactionReceiptMined');

contract('VolAirCoinCrowdsale', async function ([tokenWallet, wallet, investor, crowdsaleWallet]) {
  const volPerEth = 2500;
  const rate = new BigNumber(volPerEth);
  const value = ether(1);
  const expectedTokenAmount = rate.mul(value);
  const tokenAllowance = new BigNumber('5e+26');
  const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000';  
  const owner = tokenWallet;

  beforeEach(async function () {
    this.openingTime = latestTime() + duration.weeks(1);
    this.closingTime = this.openingTime + duration.weeks(2);
    this.afterClosingTime = this.closingTime + duration.seconds(1);

    this.token = await VolAirCoin.new({ from: owner });
    this.crowdsale = await CrowdsaleContract.new(this.openingTime, this.closingTime, rate, owner, this.token.address, crowdsaleWallet);

    //await this.crowdsale.transferOwnership(owner);

    // Transfer 250M to crowdsale account
    await this.token.transfer(crowdsaleWallet, tokenAllowance.dividedBy(2), { from: owner });

    let balance = await this.token.balanceOf(crowdsaleWallet);
    balance.should.be.bignumber.equal(tokenAllowance.dividedBy(2));

    await this.token.approve(this.crowdsale.address, tokenAllowance.dividedBy(2), { from: crowdsaleWallet });    
  });

  describe('accepting payments', function () {
    it('should accept sends', async function () {
      await increaseTimeTo(this.openingTime);
      await this.crowdsale.send(value).should.be.fulfilled;
    });

    it('should accept payments', async function () {
      await increaseTimeTo(this.openingTime);
      await this.crowdsale.buyTokens(investor, { value: value, from: investor }).should.be.fulfilled;
    });
    
    it(`should convert 1 ETH to ${volPerEth} VOL`, async function () {
      await increaseTimeTo(this.openingTime);
      let balanceBefore = await this.token.balanceOf(investor);
      await this.crowdsale.buyTokens(investor, { value: value, from: investor });
      let balance = await this.token.balanceOf(investor);
      balance.should.be.bignumber.equal(volPerEth * (10 ** 18));
    });

    it(`should fallback to buy when sent to contract address`, async function () {
      await increaseTimeTo(this.openingTime);
      let balanceBefore = await this.token.balanceOf(investor);
      var tx = await web3.eth.sendTransaction({from:investor, to:this.crowdsale.address, value: value.mul(1), gas: 100000});

      var receipt = await web3.eth.getTransactionReceiptMined(tx);
      receipt.status.should.equal('0x1');

      let balance = await this.token.balanceOf(investor);
      balance.should.be.bignumber.equal(volPerEth * (10 ** 18));
    });

    it('should reject payments before start', async function () {
      await this.crowdsale.send(value).should.be.rejectedWith(EVMRevert);
      await this.crowdsale.buyTokens(investor, { from: investor, value: value }).should.be.rejectedWith(EVMRevert);
    });

    it('should accept payments after start', async function () {
      await increaseTimeTo(this.openingTime);
      await this.crowdsale.send(value).should.be.fulfilled;
      await this.crowdsale.buyTokens(investor, { value: value, from: investor }).should.be.fulfilled;
    });

    it('should reject payments after end', async function () {
      await increaseTimeTo(this.afterClosingTime);
      await this.crowdsale.send(value).should.be.rejectedWith(EVMRevert);
      await this.crowdsale.buyTokens(investor, { value: value, from: investor }).should.be.rejectedWith(EVMRevert);
    });
  });

  describe('high-level purchase', function () {
    beforeEach(async function() {
      await increaseTimeTo(this.openingTime);
    });

    it('should log purchase', async function () {
      const { logs } = await this.crowdsale.sendTransaction({ value: value, from: investor });
      const event = logs.find(e => e.event === 'TokenPurchase');
      should.exist(event);
      event.args.purchaser.should.equal(investor);
      event.args.beneficiary.should.equal(investor);
      event.args.value.should.be.bignumber.equal(value);
      event.args.amount.should.be.bignumber.equal(expectedTokenAmount);
    });

    it('should assign tokens to sender', async function () {
      await this.crowdsale.sendTransaction({ value: value, from: investor });
      let balance = await this.token.balanceOf(investor);
      balance.should.be.bignumber.equal(expectedTokenAmount);
    });

    it('should forward funds to wallet', async function () {
      const pre = web3.eth.getBalance(owner);
      await this.crowdsale.sendTransaction({ value, from: investor });
      const post = web3.eth.getBalance(owner);
      post.minus(pre).should.be.bignumber.equal(value);
    });
  });

  describe('check remaining allowance', function () {
    it('should report correct allowace left', async function () {
      await increaseTimeTo(this.openingTime);
      let remainingAllowance = tokenAllowance - expectedTokenAmount;
      await this.crowdsale.buyTokens(investor, { value: value, from: investor });
      let tokensRemaining = await this.crowdsale.remainingTokens();
      tokensRemaining.should.be.bignumber.equal(remainingAllowance);
    });
  });

  describe('when token wallet is different from token address', function () {
    it('creation reverts', async function () {
      await increaseTimeTo(this.openingTime);
      this.token = await VolAirCoin.new({ from: tokenWallet });
      CrowdsaleContract.new(this.openingTime, this.closingTime, rate, wallet, this.token.address, ZERO_ADDRESS).should.be.rejectedWith(EVMRevert);
    });
  });

  // describe('is finalizable', function() {
  //   it('cannot be finalized before ending', async function () {
  //     await this.crowdsale.finalize({ from: owner }).should.be.rejectedWith(EVMRevert);
  //   });
  
  //   it('cannot be finalized by third party after ending', async function () {
  //     await increaseTimeTo(this.afterClosingTime);
  //     await this.crowdsale.finalize({ from: thirdparty }).should.be.rejectedWith(EVMRevert);
  //   });
  
  //   it('can be finalized by owner after ending', async function () {
  //     await increaseTimeTo(this.afterClosingTime);
  //     await this.crowdsale.finalize({ from: owner }).should.be.fulfilled;
  //   });
  
  //   it('cannot be finalized twice', async function () {
  //     await increaseTimeTo(this.afterClosingTime);
  //     await this.crowdsale.finalize({ from: owner });
  //     await this.crowdsale.finalize({ from: owner }).should.be.rejectedWith(EVMRevert);
  //   });
  
  //   it('logs finalized', async function () {
  //     await increaseTimeTo(this.afterClosingTime);
  //     const { logs } = await this.crowdsale.finalize({ from: owner });
  //     const event = logs.find(e => e.event === 'Finalized');
  //     should.exist(event);
  //   });

  //   it('transfers remaining tokens back when finalized', async function () {
  //     await increaseTimeTo(this.openingTime);
  //     await this.crowdsale.sendTransaction({ value: value, from: investor });

  //     const ownerBalancePre = await this.token.balanceOf(owner);
  //     const walletBalancePre = await this.token.balanceOf(wallet);

  //     await increaseTimeTo(this.afterClosingTime);
  //     await this.crowdsale.finalize({ from: owner });

  //     const ownerBalancePost = await this.token.balanceOf(owner);
  //     const walletBalancePost = await this.token.balanceOf(wallet);

  //     ownerBalancePre.should.be.bignumber.above(0);
  //     ownerBalancePost.should.be.bignumber.equal(0);
  //     walletBalancePost.should.be.bignumber.equal(walletBalancePre.add(ownerBalancePre));
  //   });
  // })
});

function ether(n) {
  return new web3.BigNumber(web3.toWei(n, 'ether'));
}
