pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/VolAirCoin.sol";
import "../contracts/VolAirCoinCrowdsale.sol";

contract TestVolAirCoin {
    VolAirCoin vol;

    function testInitialBalanceUsingDeployedContract() public {
        vol = VolAirCoin(DeployedAddresses.VolAirCoin());

        uint expected = 500000000 * (10 ** 18);

        Assert.equal(vol.balanceOf(msg.sender), expected, "Owner should have 500000000 VolAirCoin initially");
    }
}
