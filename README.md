# VolAir Coin Smart Contracts

The official VolAir Coin smart contract repository. Luxury Living to the people! Private Jet Charter with VolAir Coin. https://volair.io/

## Prerequisites

```
npm install -g truffle

git clone https://gitlab.com/volair/volaircoin.git
npm install
```

Local development network expects Ganache (https://truffleframework.com/ganache) on port 7545.

## Compiling and Testing

```
truffle compile
truffle test
```

## Migrations

Run local geth node.

```
geth attach http://127.0.0.1:8545

> personal.unlockAccount("0xa87fe66513beb69c13aaefdb2091d567d1f4acd3", undefined, 6000)

truffle migrate --network ropsten
```

# License

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Portions of this project use OpenZeppelin-Solidity (https://openzeppelin.org/) which is licensed under the [MIT License](https://github.com/OpenZeppelin/openzeppelin-solidity/blob/master/LICENSE)